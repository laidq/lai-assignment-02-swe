# FUNiX Passport
## Mã học viên: 10104
## Repo này dùng để quản lý source code chính của hệ thống FUNiX Passport
- Repo này gồm có 5 branch:
  - Branch master: source code chính của dự án
  - Branch 1 bug/clear_console_log: xóa các console clear_console_log
  - Branch 2 feat/auto_enable_subtitle: sửa code tự động hiển thị phụ đề
  - Branch 3 document : tạo tài liệu cho dự án
  - Branch 4 backend: tạo thư mục tài liệu cho backend
